const { name } = require('./package.json');
const bull = require('bull');

const redis = {
    host: 'localhost',
    port: 6379,
}

const opts = { redis: {host: redis.host , port: redis.port }}

const queueCreate = bull(`${name.replace('api-','')}:create`, opts)
const queueDelete = bull(`${name.replace('api-','')}:delete`, opts)
const queueUpdate = bull(`${name.replace('api-','')}:update`, opts)
const queueFindOne = bull(`${name.replace('api-','')}:findOne`, opts)
const queueView = bull(`${name.replace('api-','')}:view`, opts)


async function Create({ title,image }){
    try {
        const job = await queueCreate.add({ title, image })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }){
    try {
        const job = await queueDelete.add({ id })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({title}){
    try {
        const job = await queueFindOne.add({title})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Update({ id, title, category, seccions }){
    try {
        const job = await queueUpdate.add({id, title, category, seccions})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function View({}){
    try {

        const job = await queueView.add({})

        const {statusCode, data, message} = await job.finished()

        console.log({statusCode, data, message});

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function main() {

    //await Create({name: 'Maria', age:27, color: 'Rojo'})

    //await Delete({ id: 2})

    //await Update({age:30, id:1})

    await FindOne({id:1})

    //await View({});
}

//main()

module.exports = { Create, Delete, Update, FindOne, View }

