const api = require('./api');

async function main(){
    try {
        let { data, statusCode, message } = await api.Create( {image: 'https://nodejs.org/static/images/logo.svg', title: 'Aprende nodejs'});
        console.log({ data, statusCode, message });
    } catch (error) {
        console.log(error);
    }
}

main()
