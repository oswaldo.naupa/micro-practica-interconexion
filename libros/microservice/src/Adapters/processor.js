
const Services = require('../Services');
const { internalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queueUpdate, queueFindOne } = require('./index')

async function  View (job, done){

    try {

        const { } = job.data;

        let {statusCode,data,message} = await Services.View({});

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueView',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Create (job, done){

    try {

        const { title, image } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Create({ title, image });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueCreate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Delete(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Delete({ id });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueDelete',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function FindOne(job, done){

    try {

        const { title } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.FindOne({title});

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueFindOne',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Update(job, done){

    try {

        const { id, title, category, seccions } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Update({ id, title, category, seccions });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueUpdate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function run(){
    try {
        console.log("VAmos a inicializar worker");

        queueView.process(View);

        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueFindOne.process(FindOne);

        queueUpdate.process(Update);

    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    View, Create, Delete, Update, FindOne, run
}