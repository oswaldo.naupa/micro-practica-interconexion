

const Controllers = require('../Controllers');
const { internalError } = require('../settings');

async function Create({ title,image }){

    try {

        let { statusCode, data, message } = await Controllers.Create({ title, image })

        return { statusCode, data, message }        

    } catch (error) {
        //console.log(error);
        console.log({step:'service Create',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

async function Delete({ id}){

    try {

        const findOne = await Controllers.FindOne({where: { id }});

        if(findOne.statusCode !== 200) {

            /*let response = {
                400: {statusCode: 400, message: "No existe el usuario a eliminar"},
                500: {statusCode: 500, message: internalError} 
            }
            return response[findOne.statusCode]*/

            switch(findOne.statusCode){
                case 400: return {statusCode: 400, message: "No existe el usuario a eliminar"}
                case 500: return {statusCode: 500, message: internalError} 
                default: return {statusCode: findOne.statusCode, message: findOne.message} 
            }

        }

        const del = await Controllers.Delete({ where: {id} })

        if(del.statusCode === 200) return { statusCode:200, data: findOne.data };
        
        return { statusCode:200, message: internalError }

    } catch (error) {
        //console.log(error);
        console.log({step:'service Delete',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

async function Update({ id, title, category, seccions  }){

    try {

        let { statusCode, data, message } = await Controllers.Update({ id, title, category, seccions  })

        return { statusCode, data, message }        

    } catch (error) {
        //console.log(error);
        console.log({step:'service Update',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

async function FindOne({ title }){

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where:{ title} })

        return { statusCode, data, message }        

    } catch (error) {
        //console.log(error);
        console.log({step:'service FindOne',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

async function View({}){

    try {

        let { statusCode, data, message } = await Controllers.View({})

        return { statusCode, data, message }        

    } catch (error) {
        //console.log(error);
        console.log({step:'service View',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

module.exports = { Create, Update, Delete, FindOne, View }