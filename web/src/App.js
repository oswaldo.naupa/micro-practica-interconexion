import React from 'react';

import styled from 'styled-components';

import Socios from './Socios';
import Libros from './Libros';
import Pagos from './Pagos';

import Card from 'card';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  min-height: calc(100vh);
`

const Slider = styled.div`
  width: 35%;
  min-width:350px;
`

const Body = styled.div`
  width: 65%;
  min-width:350px;
`

function App() {

  return (
    <Container>
      <Card image={"https://www.nombresdegatos.eu/wp-content/uploads/2021/02/nombres-para-gatos-color-caf%C3%A9.jpg"}
            title={"Novedades de la app móvil"}
            labelBtn={"Instalar App"}
            description={"El gato doméstico​​, llamado más comúnmente gato, y de forma coloquial minino, ​ michino, ​ michi, ​ micho, ​ mizo, ​ miz, ​ morroño​ o morrongo, ​ y algunos nombres más."}/>
            
      {/*<Slider>
        <Socios></Socios>
        <Pagos></Pagos>
      </Slider>
      <Body>
        <Libros></Libros>
  </Body>*/}
    </Container>
  );
}

export default App;
