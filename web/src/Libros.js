import React, { useEffect, useState, version } from 'react';

import { socket } from './ws';

import styled from 'styled-components';

const Container = styled.div`
    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-self: center;
    align-content: stretch;
    display: flex;
    width: 100%;
`

const Libro = styled.div`
    background-color: #303536;
    margin-bottom:15px;
    border-radius: 10px;
    padding: 5px;
    display:flex;
    position:relative;
`

const Portada = styled.img`
    width: 200px;
`

const Icon = styled.img`
    width: 35px;
    height:35px;
    position:absolute;
    top:-6px;
    right: -5px;
    border-radius:50%;
    cursor: pointer;
`

const Title = styled.p`
    color:white;
    text-align:center;
`
const Button = styled.button`
    background-color:#036e00;
    color: white;
    top:50px;
    right: 50px;
    padding: 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    width: 100%;
    height: 50px;
    margin-bottom: 20px;
`

const Item = ({ image, title, id }) => {
    const handleDelete = () => {
        socket.emit('req:libros:delete', { id })
    }

    const opts = {
        src: "https://e7.pngegg.com/pngimages/28/52/png-clipart-button-computer-icons-cancel-button-logo-sign.png",
        onClick: handleDelete
    }

    return (
        <Libro>
            
            <Icon {...opts}/>
            <Portada src={image}/>
            <Title>{title}</Title>
        </Libro>
    )
}

const App = () => {

    const [data, setData] = useState([]);

    const handleCreate = () => {
        socket.emit('req:libros:create', { 
            image: 'https://nodejs.org/static/images/logo.svg', title: 'Aprende nodejs'
        })
    }
  
      useEffect(() => {
  
          socket.on('res:libros:view', ({statusCode, data, message})=>{
      
            console.log('res:libros:view', {statusCode, data, message});
      
            if(statusCode === 200) setData(data)
      
          })
      
          socket.emit('req:libros:view', ({}))
          //setInterval(() => socket.emit('req:microservice:view', ({})), 1500);
      
        }, [])

    return (
        <Container>
            <Button onClick={handleCreate}>Crear</Button>
            {   
                data.map((v,i) => <Item {...v}/>)
            }
        </Container>
    )
}

export default App;