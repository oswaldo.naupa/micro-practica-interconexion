import React, { useEffect, useState } from 'react';

import { socket } from './ws';

import styled from 'styled-components';

const Container = styled.div`
  max-width: 350px;
  width: 350px;
`

const ContainerBody = styled.div`
  height: 350px;
  background-color: black;
  padding: 10px;
  overflow: scroll;
`

const Socio = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 15px;
  background-color: aliceblue;
  position:relative;
 
`

const Email = styled.p`
  
`

const Body = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`
const Name = styled.p`
  color: #333;
`


const Button = styled.button`
    background-color:#F34336;
    color: white;
    top:50px;
    right: 50px;
    padding: 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    width: 100%;
    height: 50px;
    margin-bottom: 20px;
`

const Icon = styled.img`
    width: 30px;
    height:30px;
    border-radius:50%;
    cursor: pointer;
    margin-left:10px;
`

const Input = styled.input`
    width: 90%;
    height:25px;
    margin-left:10px;
    border-radius: 2px;
    margin-top:5px;
    margin-bottom:5px;
`



const App = () => {

    const [data, setData] = useState([]);

    const [value, setValue] = useState();

    const handleCreate = () => value > 0 ? socket.emit('req:pagos:create', { socio: value , amount: 300}) : null;

    const handleDelete = (id) => {
        socket.emit('req:pagos:delete', { id })
    }


    useEffect(() => {

        socket.on('res:pagos:view', ({statusCode, data, message})=>{
    
          console.log('res:pagos:view', {statusCode, data, message});
    
          if(statusCode === 200) setData(data)
    
        })
    
        socket.emit('req:pagos:view', ({ }))
        //setInterval(() => socket.emit('req:microservice:view', ({})), 1500);
    
      }, []);

      const handleInput = (e) => {
          console.log(e.target.value);
          setValue(e.target.value)
      }

    return (
        <Container>

            <Input type={'number'} onChange={handleInput}></Input>   

            <Button onClick={handleCreate}>Aplicar pago al socio {value}</Button>
        
        
            <ContainerBody>
          
            {
                data.map((v, i)=> 
                (
                <Socio>
                    
                    <Body>
                        <Name>Cupon de pago {v.id}</Name>
                        <Name>Socio: {v.socio}</Name>
                    </Body>

                    <Body>
                        <Email>{v.createdAt}</Email>
                        <Icon  src={"https://e7.pngegg.com/pngimages/28/52/png-clipart-button-computer-icons-cancel-button-logo-sign.png"} onClick={()=> handleDelete(v.id)} />
                    </Body>
                </Socio>
                ))
            }

            </ContainerBody>
      </Container>
    )
}

export default App