import React, { useEffect, useState } from 'react';

import { socket } from './ws';

import styled from 'styled-components';

const Container = styled.div`
  max-width: 350px;
  width: 350px;
`

const ContainerBody = styled.div`
  height: 350px;
  background-color: black;
  padding: 10px;
  overflow: scroll;
`

const Socio = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 15px;
  background-color: aliceblue;
  position:relative;
 
`

const Body = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`
const Name = styled.p`
  color: #333;
`
const Phone = styled.p`
  
`

const Email = styled.p`
  
`

const Enable = styled.div`
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background-color: ${props => props.enable ? `green`: `red`};
  opacity: 0.5
`

const Button = styled.button`
    background-color:#F34336;
    color: white;
    top:50px;
    right: 50px;
    padding: 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    width: 100%;
    height: 50px;
    margin-bottom: 20px;
`

const Icon = styled.img`
    width: 30px;
    height:30px;
    border-radius:50%;
    cursor: pointer;
    margin-left:10px;
`

const FeedBack = styled.div`
    display:flex;
    flex-direction:row;
    align-items:center;
`

const App = () => {

    const [data, setData] = useState([]);

    const handleCreate = () => {
      socket.emit('req:socios:create', { 
          name: 'Oswaldo', phone: '554478'
      })
  }

  const handleDelete = (id) => {
    socket.emit('req:socios:delete', { id })
}

const handleChange = (id,status) => {
  if(status) socket.emit('req:socios:disable', { id })
  else socket.emit('req:socios:enable', { id })
  
}


    useEffect(() => {

        socket.on('res:socios:view', ({statusCode, data, message})=>{
    
          console.log('res:socios:view', {statusCode, data, message});
    
          if(statusCode === 200) setData(data)
    
        })
    
        socket.emit('req:socios:view', ({ }))
        //setInterval(() => socket.emit('req:microservice:view', ({})), 1500);
    
      }, [])
      

    return(
      <Container>
        <Button onClick={handleCreate}>Create</Button>
      
      
        <ContainerBody>
          
        {
            data.map((v, i)=> 
            (
              <Socio>
                
                <Body>
                  <Name>{v.name} <small>{v.id}</small></Name>
                  <Phone>{v.phone}</Phone>
                </Body>

                <Body>
                  <Email>{v.email}</Email>
                  <FeedBack>
                    <Enable enable={v.enable} onClick={()=> handleChange(v.id, v.enable)}>{v.enable}</Enable>
                    <Icon  src={"https://e7.pngegg.com/pngimages/28/52/png-clipart-button-computer-icons-cancel-button-logo-sign.png"} onClick={()=> handleDelete(v.id)} />
                  </FeedBack>
                 
                </Body>
              </Socio>
            ))
        }

        </ContainerBody>
      </Container>
    )
}

export default App;