const { name } = require('./package.json');
const bull = require('bull');

const redis = {
    host: 'localhost',
    port: 6379,
}

const opts = { redis: {host: redis.host , port: redis.port }}

const queueCreate = bull(`${name.replace('api-','')}:create`, opts)
const queueDelete = bull(`${name.replace('api-','')}:delete`, opts)
const queueUpdate = bull(`${name.replace('api-','')}:update`, opts)
const queueFindOne = bull(`${name.replace('api-','')}:findOne`, opts)
const queueView = bull(`${name.replace('api-','')}:view`, opts)
const queueEnable = bull(`${name.replace('api-','')}:enable`, opts)
const queueDisable = bull(`${name.replace('api-','')}:disable`, opts)


async function Enable({ id }){
    try {
        const job = await queueEnable.add({ id })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Disable({ id }){
    try {
        const job = await queueDisable.add({ id})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Create({ name, phone }){
    try {
        const job = await queueCreate.add({ name, phone })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }){
    try {
        const job = await queueDelete.add({ id })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({id}){
    try {
        const job = await queueFindOne.add({id})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Update({ id, name, age, email, phone }){
    try {
        const job = await queueUpdate.add({ id, name, age, email, phone})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function View({ enable }){
    try {
        const job = await queueView.add({ enable })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function main() {

    //await Create({name: 'Maria', age:27, color: 'Rojo'})

    //await Delete({ id: 2})

    //await Update({age:30, id:1})

    await FindOne({id:1})

    //await View({});
}

//main()

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable }

