## API de pagos

Esta es una api para interactuar con los socios registrados en la biblioteca

```js
const apiSocios = require('api-socio');

socket.on('req:socios:view', async ({ enable }) => {

    try {
        console.log("req:socios:view'", {enable});

        const {statusCode, data, message} = await apiSocios.View({ enable });
    
        return io.to(socket.id).emit('res:socios:view',{statusCode, data, message});

    } catch (error) {
        console.log(error);
    }

    
})

socket.on('req:socios:create', async ({name, phone }) => {

    try{

        console.log('req:socios:create', {name, phone});

        const {statusCode, data, message} = await apiSocios.Create({ name, phone});
    
        return io.to(socket.id).emit('res:socios:create',{statusCode, data, message});

    } catch (error) {
        console.log(error);
    }

})

socket.on('req:socios:update', async ( { id,name, age, phone, email }) => {

    try {
        console.log('req:socios:update', { id,name, age, phone, email});

        const {statusCode, data, message} = await apiSocios.Update({id,name, age, phone, email});
    
        return io.to(socket.id).emit('res:socios:update',{statusCode, data, message});

    } catch (error) {
        console.log(error);
    }

})

socket.on('req:socios:delete', async ({id}) => {

    try {

        console.log('req:socios:delete', {id});

        const {statusCode, data, message} = await apiSocios.Delete({id});
    
        return io.to(socket.id).emit('res:socios:delete',{statusCode, data, message});

    } catch (error) {
        console.log(error);
    }

})

socket.on('req:socios:findOne', async ({id}) => {

    try {

        console.log('req:socios:findOne', {id});

        const {statusCode, data, message} = await apiSocios.FindOne({id});
    
        return io.to(socket.id).emit('res:socios:findOne',{statusCode, data, message});

    } catch (error) {
        console.log(error);
    }

})

socket.on('req:socios:enable', async ({id}) => {

    try {

        console.log('req:socios:enable', {id});

        const {statusCode, data, message} = await apiSocios.Enable({id});
    
        return io.to(socket.id).emit('res:socios:enable',{statusCode, data, message});

    } catch (error) {
        console.log(error);
    }

})

socket.on('req:socios:disable', async ({id}) => {

    try {

        console.log('req:socios:disable', {id});

        const {statusCode, data, message} = await apiSocios.Disable({id});
    
        return io.to(socket.id).emit('res:socios:disable',{statusCode, data, message});

    } catch (error) {
        console.log(error);
    }

})

```
Solo se puede registrar socios con nombre y telefono