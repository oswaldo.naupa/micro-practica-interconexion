
const Services = require('../Services');
const { internalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queueUpdate, queueFindOne, queueEnable, queueDisable } = require('./index')

async function  View (job, done){

    try {

        const { enable } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.View({ enable });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueView',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Create (job, done){

    try {

        const { name, phone } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Create({ name, phone });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueCreate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Delete(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Delete({ id });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueDelete',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function FindOne(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.FindOne({ id });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueFindOne',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Update(job, done){

    try {

        const { id, name, age, phone, email } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Update({ id, name, age, phone, email });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueUpdate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Enable(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Enable({ id });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueUpdate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Disable(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Disable({ id });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueUpdate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function run(){
    try {

        console.log("VAmos a inicializar worker");

        queueView.process(View);

        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueFindOne.process(FindOne);

        queueUpdate.process(Update);

        queueEnable.process(Enable);

        queueDisable.process(Disable);

    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    View, Create, Delete, Update, FindOne, Enable, Disable, run
}