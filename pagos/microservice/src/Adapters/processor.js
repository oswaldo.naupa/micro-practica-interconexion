
const Services = require('../Services');
const { internalError } = require('../settings');

const { queueView, queueCreate, queueDelete, queueFindOne } = require('./index')

async function  View (job, done){

    try {

        const { } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.View({});

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueView',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Create (job, done){

    try {

        const { socio, amount} = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Create({ socio, amount });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueCreate',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function Delete(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.Delete({ id });

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueDelete',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function FindOne(job, done){

    try {

        const { id } = job.data;

        console.log(job.id);

        let {statusCode,data,message} = await Services.FindOne({id});

        done(null, { statusCode,data,message });
        
    } catch (error) {
        console.log({step:'adapter queueFindOne',error: error.toString()});
        done(null, { statusCode:500, message: internalError});
    }

}

async function run(){
    try {

        console.log("VAmos a inicializar worker");

        queueView.process(View);

        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueFindOne.process(FindOne);


    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    View, Create, Delete, FindOne, run
}