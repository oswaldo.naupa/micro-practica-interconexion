const { Model } = require('../Models');

async function Create({socio, amount}){
    try {
        
        let instance = await Model.create(
            { socio, amount }, 
            {fields: ['socio', 'amount' ], logging:false})

        return {statusCode: 200, data: instance.toJSON() }

    } catch (error) {
        console.log({step: 'controller Create', error: error.toString()});

        return { statusCode: 500, message: error.toString() }
    }
}

async function Delete({ where = {} }){
    try {
        
        await Model.destroy({where, logging:false})

        return {statusCode: 200, data: 'OK' }

    } catch (error) {
        console.log({step: 'controller Delete', error: error.toString()});

        return { statusCode: 500, message: error.toString() }
    }
}

async function FindOne({ where = {} }){
    try {
        
        let instance = await Model.findOne({ where, logging:false })

        if (instance) return {statusCode: 200, data: instance.toJSON() };

        else return { statusCode: 400, message: "No existe el usuario"}

    } catch (error) {
        console.log({step: 'controller FindOne', error: error.toString()});

        return { statusCode: 500, message: error.toString() }
    }
}

async function View({ where = {} }){
    try {
        
        let instances = await Model.findAll({ where, logging:false })

        return {statusCode: 200, data: instances }

    } catch (error) {
        console.log({step: 'controller View', error: error.toString()});

        return { statusCode: 500, message: error.toString() }
    }
}




module.exports = { Create, Delete, FindOne, View }