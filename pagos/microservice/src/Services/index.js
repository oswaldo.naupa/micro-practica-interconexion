

const Controllers = require('../Controllers');
const { internalError, redis } = require('../settings');

const socios  = require('api-socio');

async function Create({ socio, amount}){

    try {

        const validSocio = await socios.FindOne({id: socio}, redis);

        if(validSocio.statusCode !== 200 ){
            switch(validSocio.statusCode){
                case 400: {
                    return {statusCode: 400, message: 'No existe el usuario'}
                }

                default: return {statusCode: 400, message: internalError}
            }
        }
        if(!validSocio.data.enable) return {statusCode: 400, message: 'El usuario no esta habilitado'}

        let { statusCode, data, message } = await Controllers.Create({ socio, amount })

        return { statusCode, data, message }        

    } catch (error) {
        //console.log(error);
        console.log({step:'service Create',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

async function Delete({ id}){

    try {

        const findOne = await Controllers.FindOne({where: { id }});

        if(findOne.statusCode !== 200) {

            switch(findOne.statusCode){
                case 400: return {statusCode: 400, message: "No existe el usuario a eliminar"}
                case 500: return {statusCode: 500, message: internalError} 
                default: return {statusCode: findOne.statusCode, message: findOne.message} 
            }

        }

        const del = await Controllers.Delete({ where: {id} })

        if(del.statusCode === 200) return { statusCode:200, data: findOne.data };
        
        return { statusCode:200, message: internalError }

    } catch (error) {
        //console.log(error);
        console.log({step:'service Delete',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

async function FindOne({ id }){

    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where:{ id} })

        return { statusCode, data, message }        

    } catch (error) {
        //console.log(error);
        console.log({step:'service FindOne',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

async function View({}){

    try {

        let { statusCode, data, message } = await Controllers.View({})

        return { statusCode, data, message }        

    } catch (error) {
        //console.log(error);
        console.log({step:'service View',error:error.toString()});
        return {statusCode:500,data:null,message: error.toString()}
        
    }
}

module.exports = { Create, Delete, FindOne, View }