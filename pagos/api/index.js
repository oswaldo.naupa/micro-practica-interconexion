const { name } = require('./package.json');
const bull = require('bull');

const redis = {
    host: 'localhost',
    port: 6379,
}

const opts = { redis: {host: redis.host , port: redis.port }}

const queueCreate = bull(`${name.replace('api-','')}:create`, opts)
const queueDelete = bull(`${name.replace('api-','')}:delete`, opts)
const queueFindOne = bull(`${name.replace('api-','')}:findOne`, opts)
const queueView = bull(`${name.replace('api-','')}:view`, opts)


async function Create({socio, amount}){
    try {
        const job = await queueCreate.add({socio, amount })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }){
    try {
        const job = await queueDelete.add({ id })

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({id}){
    try {
        const job = await queueFindOne.add({id})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}


async function View({}){
    try {
        const job = await queueView.add({})

        const {statusCode, data, message} = await job.finished()

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function main() {

    //await Create({name: 'Maria', age:27, color: 'Rojo'})

    //await Delete({ id: 2})

    //await Update({age:30, id:1})

    await FindOne({id:1})

    //await View({});
}

//main()

module.exports = { Create, Delete, FindOne, View }

