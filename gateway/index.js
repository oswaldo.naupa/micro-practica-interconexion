const express = require('express');
const http = require('http');

const app = express();

const server = http.createServer(app);

const { Server } = require('socket.io');

const io = new Server(server);


const apiLibros = require('api-libro');
const apiSocios = require('api-socio');
const apiPagos = require('api-pago');


server.listen(80, () => {
    console.log("server initialize");

    io.on('connection',socket =>{
        console.log("New connection", socket.id);

        // libros

        socket.on('req:libros:view', async ({}) => {

            try {
                console.log("req:libros:view");

                const {statusCode, data, message} = await apiLibros.View({});

                console.log('ayuda',{statusCode, data, message});
            
                return io.to(socket.id).emit('res:libros:view',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

            
        })

        socket.on('req:libros:create', async ({title, image }) => {

            try{

                console.log('req:libros:create', {title , image});

                const {statusCode, data, message} = await apiLibros.Create({ title, image });
            
                io.to(socket.id).emit('res:libros:create',{statusCode, data, message});

                const view = await apiLibros.View({});

                return io.to(socket.id).emit('res:libros:view',view);

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:libros:update', async ( { id, title, category, seccions }) => {

            try {
                console.log('req:libros:update', { id, title, category, seccions });

                const {statusCode, data, message} = await apiLibros.Update({id, title, category, seccions});
            
                return io.to(socket.id).emit('res:libros:update',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:libros:delete', async ({id}) => {

            try {

                console.log('req:libros:delete', {id});

                const {statusCode, data, message} = await apiLibros.Delete({id});
            
                io.to(socket.id).emit('res:libros:delete',{statusCode, data, message});

                const view = await apiLibros.View({});

                return io.to(socket.id).emit('res:libros:view',view);

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:libros:findOne', async ({title}) => {

            try {

                console.log('req:libros:findOne', {title});

                const {statusCode, data, message} = await apiLibros.FindOne({title});
            
                return io.to(socket.id).emit('res:libros:findOne',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

        })

        //pagos

        socket.on('req:pagos:view', async ({}) => {

            try {
                console.log("req:pagos:view'");

                const {statusCode, data, message} = await apiPagos.View({});
            
                return io.to(socket.id).emit('res:pagos:view',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

            
        })

        socket.on('req:pagos:create', async ({socio, amount}) => {

            try{

                console.log('req:pagos:create', {socio, amount });

                const {statusCode, data, message} = await apiPagos.Create({socio, amount});
            
                io.to(socket.id).emit('res:pagos:create',{statusCode, data, message});

                const view = await apiPagos.View({});

                return io.to(socket.id).emit('res:pagos:view',view);
            
            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:pagos:delete', async ({id}) => {

            try {

                console.log('req:pagos:delete', {id});

                const {statusCode, data, message} = await apiPagos.Delete({id});
            
                io.to(socket.id).emit('res:pagos:delete',{statusCode, data, message});

                const view = await apiPagos.View({});

                return io.to(socket.id).emit('res:pagos:view',view);

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:pagos:findOne', async ({id}) => {

            try {

                console.log('req:pagos:findOne', {id});

                const {statusCode, data, message} = await apiPagos.FindOne({id});
            
                return io.to(socket.id).emit('res:pagos:findOne',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

        })

        //socios

        socket.on('req:socios:view', async ({ enable }) => {

            try {
                console.log("req:socios:view'", {enable});

                const {statusCode, data, message} = await apiSocios.View({ enable });
            
                return io.to(socket.id).emit('res:socios:view',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

            
        })

        socket.on('req:socios:create', async ({name, phone }) => {

            try{

                console.log('req:socios:create', {name, phone});

                const {statusCode, data, message} = await apiSocios.Create({ name, phone});
            
                io.to(socket.id).emit('res:socios:create',{statusCode, data, message});

                const view = await apiSocios.View({});

                return io.to(socket.id).emit('res:socios:view',view);

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:socios:update', async ( { id,name, age, phone, email }) => {

            try {
                console.log('req:socios:update', { id,name, age, phone, email});

                const {statusCode, data, message} = await apiSocios.Update({id,name, age, phone, email});
            
                return io.to(socket.id).emit('res:socios:update',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:socios:delete', async ({id}) => {

            try {

                console.log('req:socios:delete', {id});

                const {statusCode, data, message} = await apiSocios.Delete({id});
            
                io.to(socket.id).emit('res:socios:delete',{statusCode, data, message});

                const view = await apiSocios.View({});

                return io.to(socket.id).emit('res:socios:view',view);

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:socios:findOne', async ({id}) => {

            try {

                console.log('req:socios:findOne', {id});

                const {statusCode, data, message} = await apiSocios.FindOne({id});
            
                return io.to(socket.id).emit('res:socios:findOne',{statusCode, data, message});

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:socios:enable', async ({id}) => {

            try {

                console.log('req:socios:enable', {id});

                const {statusCode, data, message} = await apiSocios.Enable({id});
            
                io.to(socket.id).emit('res:socios:enable',{statusCode, data, message});

                const view = await apiSocios.View({});

                return io.to(socket.id).emit('res:socios:view',view);

            } catch (error) {
                console.log(error);
            }

        })

        socket.on('req:socios:disable', async ({id}) => {

            try {

                console.log('req:socios:disable', {id});

                const {statusCode, data, message} = await apiSocios.Disable({id});
            
                io.to(socket.id).emit('res:socios:disable',{statusCode, data, message});

                const view = await apiSocios.View({});

                return io.to(socket.id).emit('res:socios:view',view);

            } catch (error) {
                console.log(error);
            }

        })
    })
})