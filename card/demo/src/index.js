import React from 'react';
import { render } from 'react-dom';
import Example from '../../src';

const App = () => {
    return(
        <div>
            <h2>CArd demo</h2>
            <Example image={"https://www.nombresdegatos.eu/wp-content/uploads/2021/02/nombres-para-gatos-color-caf%C3%A9.jpg"}
            title={"Novedades de la app móvil"}
            labelBtn={"Instalar App"}
            description={"El gato doméstico​​, llamado más comúnmente gato, y de forma coloquial minino, ​ michino, ​ michi, ​ micho, ​ mizo, ​ miz, ​ morroño​ o morrongo, ​ y algunos nombres más, es un mamífero carnívoro de la familia Felidae."}/>
        </div>
    )
}

render(<App/>, document.querySelector('#demo'))
