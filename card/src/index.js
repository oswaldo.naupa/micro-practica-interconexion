import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction:row;
  width: 530px;
  height: 245px;
`

const Body = styled.div`
  display: flex;
  flex-direction:column;
  width: 50%;
  padding-left:10px;
`
const Title = styled.p`
  margin-top:0px;
  font-size: 24px;
  font-family: Lucida Sans;
`

const Description = styled.p`
  margin-top:-18px;
  text-align: justify;
  line-height: 22px;
  font-size: 14px;
`

const Image = styled.img`
  width: 50%;
  border-radius:18px;
  object-fit: cover;
  object-position: center center;
`

const Button = styled.button`
  width: 100%;
  height: 45px;
  border-radius:10px;
  background-color: #1F54CC;
  color:white;
  font-size: 16px;
  font-weight: bold;
  font-family: Arial, Helvetica, sans-serif;
`


const App = ( { title, description, image, labelBtn }) => {
    return(
        <Container>
            <Image src={image}/>
            <Body>
              <Title>{title}</Title>
              <Description>{description}</Description>
             { labelBtn ? <Button>{labelBtn}</Button> : null}
            </Body>
        </Container>
    )
}

export default App
